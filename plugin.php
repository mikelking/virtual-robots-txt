<?php
/*
Plugin Name: Virtual Robots.txt
Version: 1.1.6
Description: Easily edit, maintain and serve the contents for your robots.txt in a WordPress.
Author: Mikel King
Text Domain: virtual-robots-txt
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2019, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Helps inhibit cross site scripting attacks
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require( 'inc/dependencies.php' );
//require( 'vendor/autoload.php' ); /may no longer be required
require( 'inc/vrt-manager-admin.php' );
require( 'inc/vrt-role-manager.php' );

/**
 * Class Virtual_Robots_Txt_Controller
 *
 * Purpose to act as the central hub for all robots.txt operations
 *
 * @todo Get the role manager working
 */
class Virtual_Robots_Txt_Controller extends Base_Plugin {
	const VERSION      = '1.1.6';
	const FILE_SPEC    = __FILE__;
	const PRIORITY     = 0;
	const HEADER       = "###\n# Robots.txt - created by the Virtual Robots.txt WordPress plugin. \n";
	const HEADER2      = "# Source: https://gitlab.com/mikelking/virtual-robots-txt \n#\n";
	const HEADER3      = "# Plugin: https://www.wordpress.org/plugins/virtual-robots-txt/ \n#\n";
	const SPEC_SITE    = "# Specifications https://developers.google.com/search/reference/robots_txt\n\n";
	const VALIDATOR    = "# VALIDATOR: https://technicalseo.com/seo-tools/robots-txt/ \n\n";
	const FOOTER       = "\n\n# Author: Mikel King @ Olivent.com \n";
    
	public $vrt_settings;

	public function __construct() {
		$vrtma = new VRT_Manager_Admin();
		$this->vrt_settings = $vrtma->get_options();

        // This is how to add an deactivation hook if needed
        register_deactivation_hook( static::FILE_SPEC, [ $this, 'deactivator' ] );

        // This is how to add an deactivation hook if needed
        register_activation_hook( static::FILE_SPEC, [ $this, 'activator' ] );


        add_action( 'robots_txt', [ $this, 'render_robots_txt' ], static::PRIORITY );

        add_action( 'wp_loaded', [ $this, 'inhibit_yoast_seo_robots_txt' ], static::PRIORITY );
	}

    /**
     * @return void
     */
    public function render_robots_txt(): void {
		$output  = self::HEADER;
		$output .= self::HEADER2;
		$output .= self::HEADER3;
		$output .= self::SPEC_SITE;
		$output .= self::VALIDATOR;
		$output .= $this->vrt_settings['VRT_Data'];
		$output .= self::FOOTER;
		echo $output;
	}

    /**
     * @return void
     */
    public function deactivator(): void {
		$options = $this->get_options();
		if ( $options['remove_settings'] ) {
			delete_option( self::OPT_NAME );
		}
        $vrtrm = new VRT_Role_Manager();
		$vrtrm->remove_capabilities_and_roles();
	}

    /**
     * @return void
     */
    public function activator(): void {
		$vrtrm = new VRT_Role_Manager();
        $vrtrm->add_capability_to_admin();
        $vrtrm->add_custom_role();
	}

    /**
     * Fix Yoast SEO robots.txt changes.
     * https://wordpress.org/support/topic/disable-robots-txt-changing-by-yoast-seo/#post-16648736
     */
    public function inhibit_yoast_seo_robots_txt() {
        global $wp_filter;

        if ( isset( $wp_filter['robots_txt']->callbacks ) && is_array( $wp_filter['robots_txt']->callbacks ) ) {
            foreach ( $wp_filter['robots_txt']->callbacks as $callback_priority => $callback ) {
                foreach ( $callback as $function_key => $function ) {
                    if ( 'filter_robots' === $function['function'][1] ) {
                        unset( $wp_filter['robots_txt']->callbacks[ $callback_priority ][ $function_key ] );
                    }
                }
            }
        }
    }
}

Virtual_Robots_Txt_Controller::get_instance();


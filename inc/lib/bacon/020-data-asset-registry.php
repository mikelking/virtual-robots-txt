<?php
/*
Plugin Name: Data Asset Registry Class
Version: 1.0.1
Description: Sets a standard class to build new plugin from.
Author: Mikel King
Text Domain: data-asset-registry
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2021, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Class Data_Asset_Registry - instantiate this class passing in a keyed array. The primary key
 * will be extracted and used as the variable name for the localization.
 *
 * EXAMPLE:
 *
 * $asset = [
 *     'primaryKey' => [
 *         'dataKey1' => 'dataValue1',
 *         'dataKey2' => 'dataValue2',
 *         'dataKey3' => 3,
 *     ]
 * ];
 *
 * <!-- Localized data asset head HOOK -->
 * <script type='text/javascript' id='localized_data_asset_head'>
 *     var primaryKey = {"dataKey1":"dataValue1","dataKey2":"dataValue2","dataKey3":3};
 * </script>
 *
 * @TODO Add check to ensure that preexisting key are not clobbered but some sort of logical array splicing is applied.
 *
 */
Class Data_Asset_Registry extends Singleton_Base {
	const INDENT   = '    ';
	const PRIORITY = 99;

	private static $registry = [];
	private static $printed = false;

	public function __construct( array $asset ) {
		$this->register_asset( $asset );
		/**
		 * Set the to 99 so that it fires as late as possible in the chain, giving
		 * all od the registration methods an opportunity to complete.
		 */
		add_action( 'localized_data_asset_head', [ $this, 'localize_data_asset' ], self::PRIORITY  );
	}

	/**
	 * The primary key of the asset array will become the
	 * referenced var in the localized JavaScript entity.
	 * @param array $asset
	 */
	public function register_asset( array $asset ) {
		self::$registry[array_key_first( $asset )] = $asset[array_key_first( $asset )];
	}

	/**
	 * Localize the data asset into the DOM
	 *
	 * @added a static to inhibit multiple printings
	 */
	public function localize_data_asset() {
		if ( self::$printed === true ) {
			return;
		}
		$script  = "<!-- Localized data asset head HOOK -->" . PHP_EOL;
		$script .= "<script type='text/javascript' id='localized_data_asset_head'>" . PHP_EOL;

		foreach ( self::$registry as $key => $data ) {
			$script .= self::INDENT . 'var ' . $key . ' = ' . json_encode( $data ) . ';' . PHP_EOL;
		}

		$script .= '</script>' .  PHP_EOL;
		print( $script );
		self::$printed = true;
	}
}

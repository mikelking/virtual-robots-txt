<?php

class VRT_Manager_Admin extends Base_Manager_Admin {
	const PAGE_TITLE       = 'VRT Manager';
	const MENU_SLUG        = 'vrt-manager';
	const METHOD_PREFIX    = 'vrt_manager';
    const WIDTH            = '99%';
    const HEIGHT           = '300px';
	const SECTION_INFO     = '<b>Note</b> Enter the Virtual Robots text relevant to this site.';
	const FIELD_FMT        = '<textarea name="%s" rows="6" id="%s" style="width:%s; height:%s;">%s</textarea>';
	const OLD_FMT          = '<input type="text" id="%s" name="%s" value="%s" style="max-width:%s; width: %s;" />';
	const PLUGIN_CREDIT    = '<h1>multo serious</h1>';
    const TICKET_URL_BASE  = 'https://gitlab.com/mikelking/virtual-robots-txt/-/issues';

	public $options;
	public $url;
	public $address;

	public $fields= array(
			'Virtual Robots Text' => 'VRT_Data',
	);

    /**
     *
     */
    public function set_maintainer_notes() {
        $notes  = "We link out to documentation from here." . PHP_EOL;
        $this->notes = $notes;
    }

    public function set_changes() {
        $this->changes = [
            '1' => 'Fixed bacon as a composer asset',
            '2' => 'Implemented SEO Manager role and capabilities',
        ];

        /**
         * Adding krsort() to automatically filter the listing from highest to lowest.
         */
        krsort( $this->changes );
    }

	/**
     * Validates the stored/user inputted field data (if any) and outputs it in the field in the CMS
	 * @param $field
	 */
	public function option_validator( $field ) {
		$option = isset( $this->options[$field] ) ? esc_attr( $this->options[$field] ) : '';
		printf(
            static::FIELD_FMT,
            static::MENU_SLUG . '-' . $field,
            static::MENU_SLUG . '-' . $field,
            static::WIDTH,
            static::HEIGHT,
            $option
		);
	}

	protected function get_address() {
		$this->url = new URL_Magick();
		$this->address = $url::$protocol . $url::PROTOCOL_DELIM . $url::$host;
		return $this->address;
	}

	public function __construct() {
		$this->get_admin_options();
        $this->set_changes();
        $this->set_maintainer_notes();
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}

	/**
	 * Prints the admin page section heading
	 */
	public function print_section_info() {
		$output =  static::SECTION_INFO;
		$output .= '<p>You can <a href="' . $this->address . '/robots.txt" target="_blank" onclick="window.open(\'' . $this->address;
		$output .= '/robots.txt\', \'popupwindow\', \'resizable=1,scrollbars=1,width=760,height=500\');return false;">';
		$output .= 'preview your robots.txt file here</a> (opens a new window). If your robots.txt file doesn\'t match what';
		$output .= ' is shown below, you may have a physical file that is being displayed instead.</p>';
		$output .= static::PLUGIN_CREDIT;
		echo wpautop( $output );
	}
}

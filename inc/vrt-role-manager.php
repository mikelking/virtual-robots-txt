<?php

/**
 * Class VRT_Role_Manager
 *
 * @see https://developer.wordpress.org/plugins/users/roles-and-capabilities/
 * @see https://developer.wordpress.org/reference/classes/wp_roles/is_role/
 * @see https://developer.wordpress.org/reference/functions/wp_roles/
 *
 * @TODO abstract the Role_Manager class into an extendable bacon base class
 */
class VRT_Role_Manager extends Role_Manager_Base {
	const ROLE_NAME  = 'SEO Manager';
	const ROLE_SLUG  = 'seo_manager';
	const CAPE_NAME  = 'edit_robots_txt';
    const ROLE_PREFIX = 'wp';
	const PRIORITY   = 11;
}


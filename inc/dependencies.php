<?php

if ( ! function_exists( 'reliable_class_exists' ) ) {
    /**
     * I hate having to write code like this class_exists() should work
     *
     * @param $class_name
     * @return bool
     */
    function reliable_class_exists( $class_name ) {
        foreach ( get_declared_classes() as $class ) {
            if ( strcasecmp( $class_name, $class ) == 0 ) {
                return ( true );
            }
        }
        return ( false );
    }
}

/**
 * If your site does not have bacon installed as a central mu-plugin library then
 * this will load the version included in the lib tree.
 * However, if your site does; then this will skip along.
 *
 * @todo switch this to use Bacon_Framework and check for the version property
 */
if ( ! reliable_class_exists( 'Base_Plugin' ) ) {
    require( 'inc/lib/bacon-loader.php' );
}